#include "sboxgen.h"

unsigned __int32 modp(unsigned __int32 a, unsigned __int16 p)
{// res = mod | res
 //    16  | 16
	unsigned __int32 res = 0;
	unsigned __int32 h = 0x8000;
	unsigned __int32 dp = p, da = a, t = 1;

	while (h > a)h >>= 1;
	while (dp < h)
	{
		dp <<= 1;
		t <<= 1;
	}
	if (a < p)
	{
		if (p == dp)
		{
			da ^= dp;
			return (da << 16) | 1;
		}
		else
			return da << 16;
	}
	while (h > p || h&p)
	{
		if (da&h)
		{
			da ^= dp;
			dp >>= 1;
			res ^= t;
		}
		h >>= 1;
		t >>= 1;
	}
	return res << 16 | da;
}
unsigned __int32 mulmod(unsigned __int16 a, unsigned __int16 b, unsigned __int16 p)
{
	unsigned __int16 da = a, db = b;
	unsigned __int32 res = 0;
	while (db)
	{
		if (db & 1)
			res ^= da;
		da <<= 1;
		db >>= 1;
	}
	return modp(res, p);
}
int KNyberg(unsigned __int16*sbox, int n, FILE *f)
{
	return 0;
}