#include "sboxgen.h"

void help()
{
	printf("Add some help here.");
}
int generate(unsigned __int16*sbox, int n, int method, FILE *seed)
{
	int res = 0;
	switch (method)
	{
	case 0: res = KNyberg(sbox, n, seed); break;
	case 1: res = DBGen(sbox, n, seed); break;
	case 2: res = Partial(sbox, n, seed); break;
	case 3: res = Factor(sbox, n, seed); break;
	default: return 1;
	}
	return res;
}

int main(int argc, char* argv[])
{
	/* Usage format:
	SBoxGen2.exe [K|F|P|D] <n> <lin> <dif> <output> <source>
		Generation methods:
			K - K.Nyberg generator
			F - Factorial generator
			P - Partial generator
			D - database
		n - S-box length, integer
		lin - maximum lin. char., integer
		dif - maximum dif.char., integer
		output - output file name
		source - randomness source, file name (for F, P, D methods) */
	unsigned __int16*sbox;
	int method = 0;

	if (argc < 6)
	{
		help();
		return ARGUMENT_ERROR;
	}
	switch (argv[1][0])
	{
	case 'K':
		if (argc < 6)
		{
			help();
			return ARGUMENT_ERROR;
		} break;
	case 'F': method++;	// 3
	case 'P': method++; // 2
	case 'D': method++; // 1
		if (argc < 7)
		{
			help();
			return ARGUMENT_ERROR;
		}
		break;
	default: return ARGUMENT_ERROR;
	}

	int n = 0, dif = 0, lin = 0;
	n = atoi(argv[2]);
	dif = atoi(argv[4]);
	lin = atoi(argv[3]);
	if (!n || !dif || !lin)
	{
		help();
		return ARGUMENT_ERROR;
	}
	int dn = n, z = 0;
	while (dn)
	{
		dn >>= 1;
		z += dn & 1;
	}
	if (z != 1)
		return INVALIDPARAM_ERROR;
	sbox = (unsigned __int16*)calloc(n, sizeof(int));
	FILE *seed, *res_sbox;
	fopen_s(&seed, argv[6], "rb");
	if (seed == NULL)
	{
		_fcloseall();
		return FILENOTFOUND_ERROR;
	}
	fopen_s(&res_sbox, argv[5], "w");
	if (res_sbox == NULL)
	{
		_fcloseall();
		return FILENOTOPENED_ERROR;
	}
	int difres, linres;
	do {
		int res = generate(sbox, n, method, seed);
		if (res)
			return res;
		difres = diftest(sbox, n);
		linres = lintest(sbox, n);
	} while (deadpoint(sbox, n) || difres > dif || linres > lin);
	for (int i = 0; i < n; i++)
	{
		printf("%x ", sbox[i]);
		fprintf(res_sbox, "%x", sbox[i]);
	}
	printf("\nFinished!\n");
	_fcloseall();
	_getch();
	return 0;
}