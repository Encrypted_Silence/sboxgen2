#include "sboxgen.h"

unsigned __int64 fact(int n)
{
	unsigned __int64 r = 1;
	for (unsigned __int64 i = 1; i <= n; i++)
		r *= i;
	return r;
}

int genbyseed(unsigned int n, unsigned __int64 seed, unsigned __int16 *sbox)
{
	if (n > 2048)
		return -1; // invalid sbox length
	unsigned __int16* lst = (unsigned __int16*)calloc(n, sizeof(unsigned __int16));
	unsigned __int16* p = (unsigned __int16*)calloc(n, sizeof(unsigned __int16));
	for (int i = 0; i < n; i++)
		lst[i] = i;
	unsigned __int64 t = seed;
	for (int i = 0; i < n - 1; i++)
	{
		unsigned __int64 nfact = fact(n - i - 1);
		unsigned __int64 x = t / nfact;
		t = t % nfact;
		//printf("%d %d\n", x, lst[x]);
		sbox[i] = lst[x];
		for (int j = x; j < n - 1; j++)
			lst[j] = lst[j + 1];
	}
	sbox[n - 1] = lst[0];
	return 0;
}

int Factor(unsigned __int16*sbox, int n, FILE* f)
{
	unsigned __int64 seed;
	if (!fread_s(&seed, sizeof(unsigned __int64), sizeof(unsigned __int64), 1, f))
		return FILEREAD_ERROR;
	seed %= fact(n);
	genbyseed(n, seed, sbox);

	return 0;
}