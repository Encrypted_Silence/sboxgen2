#pragma once
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <conio.h>

typedef enum
{
	ARGUMENT_ERROR = 1,
	FILENOTFOUND_ERROR = 2,
	FILENOTOPENED_ERROR = 3,
	INVALIDPARAM_ERROR = 4,
	FILEREAD_ERROR = 5
}Errors;
/* General */
void help();
/* S-box generators */
int KNyberg(unsigned __int16*sbox, int n, FILE *f);
int DBGen(unsigned __int16*sbox, int n, FILE* f);
int Partial(unsigned __int16*sbox, int n, FILE* f);
int Factor(unsigned __int16*sbox, int n, FILE* f);
/* Tests */
int diftest(unsigned __int16*sbox, int n);
int lintest(unsigned __int16*sbox, int n);
int deadpoint(unsigned __int16*sbox, int n);