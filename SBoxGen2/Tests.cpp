#include "sboxgen.h"

void diff(unsigned __int16* sbox, int** dt, int n)
{
	for (int a = 0; a < n; a++)
		for (int b = 1; b < n; b++)
			dt[b][sbox[a] ^ sbox[a^b]]++;
	dt[0][1] = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (dt[i][j] > dt[0][1])
			{
				dt[0][1] = dt[i][j];
				dt[0][2] = i;
				dt[0][3] = j;
			}
	/*for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			printf("%d", dt[i][j]);
		printf("\n");
	}
	printf("\n");*/
}
int diftest(unsigned __int16*sbox, int n)
{
	int**dt = (int**)calloc(n, sizeof(int*));
	for (int i = 0; i < n; i++)
		dt[i] = (int*)calloc(n, sizeof(int));
	diff(sbox, dt, n);
	int res = dt[0][1];
	for (int i = 0; i < n; i++)
		free(dt[i]);
	free(dt);
	return res;
}
int abs(int x)
{
	return x >= 0 ? x : -x;
}
int bhw(unsigned __int8 x)
{
	int w[16] = { 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0 };
	return w[x & 0xf] ^ w[x >> 4];
}
void lin(unsigned __int16* sbox, int**lt, int n)
{
	for (int a = 0; a < n; a++)
		for (int b = 0; b < n; b++)
			for (int x = 0; x < n; x++)
				lt[a][b] += bhw(a&x) == bhw(sbox[x] & b) ? 1 : -1;
	for (int i = 0; i < n; i++)
		for (int j = 1; j < n; j++)
			if (abs(lt[i][j])>abs(lt[0][1]))
			{
				lt[0][1] = lt[i][j];
				lt[0][2] = i;
				lt[0][3] = j;
			}
}
int lintest(unsigned __int16*sbox, int n)
{
	int**lt = (int**)calloc(n, sizeof(int*));
	for (int i = 0; i < n; i++)
		lt[i] = (int*)calloc(n, sizeof(int));
	lin(sbox, lt, n);
	int res = lt[0][1];
	for (int i = 0; i < n; i++)
		free(lt[i]);
	free(lt);
	return res;
}
int deadpoint(unsigned __int16*sbox,int n)
{
	for (int i = 0; i < n; i++)
		if (sbox[i] == i)
			return 1;
	return 0;
}